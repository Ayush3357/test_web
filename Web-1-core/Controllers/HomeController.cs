﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Web_1_core.Models;

namespace Web_1_core.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(Answers answers)
        {
            Response res = new Response();
            if (ModelState.IsValid)
            {
                if (answers.FirstPM.Equals("Jawahar Lal Nehru"))
                {
                    res.correct++;
                }
                if (answers.independenceyear == "1947")
                {
                    res.correct++;
                }
                if (answers.NationalAnimal == "Tiger")
                {
                    res.correct++;
                }
                if (answers.NationalFlower == "Lotus")
                {
                    res.correct++;
                }
                if (answers.President == "Ramnath Kovind")
                {
                    res.correct++;
                }
                res.percent = res.correct * 100 / res.total;
                if (res.percent >= 70)
                {
                    res.Message = "Congratulations!! you passed";
                }
                return View("Response", res);
            }
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
