﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_1_core.Models
{
    public class Answers
    {
        [Required(ErrorMessage ="Please select a response" )]
        public string independenceyear { get; set; }

        [Required(ErrorMessage = "Please select a response")]
        public string FirstPM { get; set; }

        [Required(ErrorMessage = "Please select a response")]
        public string President { get; set; }

        [Required(ErrorMessage = "Please select a response")]
        public string NationalFlower { get; set; }

        [Required(ErrorMessage = "Please select a response")]
        public string NationalAnimal { get; set; }
    }
}