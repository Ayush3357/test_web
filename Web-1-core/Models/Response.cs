﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_1_core.Models
{
    public class Response
    {
        public int total { get; set; } = 5;
        public int correct { get; set; }
        public float percent { get; set; }
        public string Message { get; set; } = "Please improve your knowledge";
    }
}