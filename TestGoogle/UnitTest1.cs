using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace TestGoogle
{
    public class Tests
    {
        ChromeDriver driver;
        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.google.com");
        }

        [Test]
        public void Test1()
        {
            driver.FindElement(By.XPath("//*[@id='tsf']/div[2]/div[1]/div[1]/div/div[2]/input")).SendKeys("Youtube" + Keys.Enter);
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='rso']/div[1]/div/div/div/div/div[1]/a/h3")).Click();
            Thread.Sleep(1000);
            Assert.IsTrue(driver.Url.Contains("youtube"));
            driver.Quit();
        }
    }
}