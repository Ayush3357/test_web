﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_1.Models;

namespace Web_1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(Answers answers)
        {
            Response res = new Response();
            if (ModelState.IsValid)
            {
                if(answers.FirstPM.Equals("Jawahar Lal Nehru"))
                {
                    res.correct++;
                }
                if(answers.independenceyear == "1947")
                {
                    res.correct++;
                }
                if(answers.NationalAnimal == "Tiger")
                {
                    res.correct++;
                }
                if(answers.NationalFlower == "Lotus")
                {
                    res.correct++;
                }
                if(answers.President == "Ramnath Kovind")
                {
                    res.correct++;
                }
                res.percent = res.correct * 100 / res.total;
                if(res.percent >= 70)
                {
                    res.Message = "Congratulations!! you passed";
                }
                return View("Response", res);
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}